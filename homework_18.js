`use strict`;


let objectOne = {
  name: "Oksana",
  chidren: {
    Misha: 11,
    Kristina: 14,
    cat: {
      age: 5 }
  },
  age: 43,
  interests: {
    history: 85,
    programming: { js: 89, css: 87},
    hiking: 8 },

};
console.log('Object One before cloning: ', objectOne);


function fullClone(obj) {
  let objectClone = {};
  for(let i in obj) {
    if (obj[i] instanceof Object) {
      objectClone[i] = fullClone(obj[i]);
      continue;
    }
    objectClone[i] = obj[i];
  }
  return objectClone;
};

let objectTwo = fullClone(objectOne);

objectTwo.music = {band: "Imagine Dragons"};

console.log('Object Two: ', objectTwo);



















































